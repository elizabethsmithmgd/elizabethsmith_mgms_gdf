
#ifndef __GAME_SCENE_H__
#define __GAME_SCENE_H__

#include "cocos2d.h"
#include "MainMenuScene.h"
#include "Box2D/Box2D.h"

using namespace cocos2d;

#define PTM_RATIO 32
#define WORLD_TO_SCREEN(n) ((n)* PTM_RATIO)
class GameScene : public cocos2d::Scene
{
public:
    static cocos2d::Scene* createScene();
    
    virtual bool init();
    
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);
    //void summonBall(cocos2d::Ref* pSender);
    //void moveUp(cocos2d::Touch *touch, cocos2d::Event *event);
    //void moveDown(cocos2d::Touch *touch, cocos2d::Event *event);
    void loadBall();
    
    void tick(float dt);
    void jump (Point p);
    bool touchBegan(Touch* touch, Event* event);
    
    
    // implement the "static create()" method manually
    CREATE_FUNC(GameScene);
};

#endif // __GAME_SCENE_H__
