#include "GameScene.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

b2World* world;
b2Vec2 gravity;
auto visibleSize = Director::getInstance()->getVisibleSize();
Vec2 origin = Director::getInstance()->getVisibleOrigin();

Scene* GameScene::createScene()
{

    return GameScene::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in GameScene.cpp\n");
}

// on "init" you need to initialize your instance
bool GameScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        printf("GameScene init returnd false");
        return false;
    }

//Add Ball to scene
    loadBall();
    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.
    
    // add a "close" icon to exit the progress. it's an autorelease object
    auto homeButton = MenuItemImage::create(
                                           "homebutton.png",
                                           "homebutton.png",
                                           CC_CALLBACK_1(GameScene::menuCloseCallback, this));
    
    if (homeButton == nullptr ||
        homeButton->getContentSize().width <= 0 ||
        homeButton->getContentSize().height <= 0)
    {
        problemLoading("'homebutton.png' and 'homebutton.png'");
    }
    else
    {
        homeButton->setScale(.2);
        float x = origin.x + visibleSize.width - homeButton->getContentSize().width/10;
        float y = origin.y + homeButton->getContentSize().height/10;
        homeButton->setPosition(Vec2(x,y));
        
    }
    
    // create menu, it's an autorelease object
    auto menu = Menu::create(homeButton, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);
    
    /////////////////////////////
    // 3. add your codes below...
    
    
    // add a label shows "Hello World"
    // create and initialize a label
    
    auto label = Label::createWithTTF("Game Scene", "fonts/Marker Felt.ttf", 24);
    if (label == nullptr)
    {
        problemLoading("'fonts/Marker Felt.ttf'");
    }
    else
    {
        // position the label on the center of the screen
        label->setPosition(Vec2(origin.x + visibleSize.width/2,
                                origin.y + visibleSize.height - label->getContentSize().height));
        
        // add the label as a child to this layer
        this->addChild(label, 1);
    }
    
    // add "HelloWorld" splash screen"
    auto background = Sprite::create("Background.png");
    if (background == nullptr)
    {
        problemLoading("'Background.png'");
    }
    else
    {
        // position the sprite on the center of the screen
        background->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
       
        background->setScale(visibleSize.width/background->getContentSize().width, visibleSize.height/background->getContentSize().height);
        
        // background->setScale(visibleSize.width/background.getLocalBounds(), visibleSize.height/background.getLocalBounds());
        
        // add the sprite as a child to this layer
        this->addChild(background, 0);
        printf("background loaded");
    }
    
    gravity.Set(0.0f, - WORLD_TO_SCREEN(9.8));
    bool doSleep = true;
    world = new b2World(gravity);
    world->SetAllowSleeping(doSleep);
    
    auto touchListner = EventListenerTouchOneByOne::create();
    touchListner->onTouchBegan = CC_CALLBACK_2(GameScene::touchBegan, this);
    getEventDispatcher()->addEventListenerWithFixedPriority(touchListner, 100);
    
    schedule(schedule_selector(GameScene::tick));
    
    
    return true;
}

bool GameScene::touchBegan(Touch* touch, Event* event){
    
    GameScene::jump(touch->getLocation());
    
    return true;
}

void GameScene::jump(Point p){
    
    
}

void GameScene::tick(float dt){
    
    int velocityIterations = 8;
    int positionIterations = 3;
    
    world->Step(dt, velocityIterations, positionIterations);
    
    for (auto b = world->GetBodyList(); b; b = b->GetNext()){
        if (b->GetUserData() != nullptr){
            auto myactor = (Sprite*) b->GetUserData();
            myactor->setPosition(Point(b->GetPosition().x, b->GetPosition().y));
            myactor->setRotation(-1 * CC_RADIANS_TO_DEGREES(b->GetAngle()));
            
            if(myactor->getPositionY() < - WORLD_TO_SCREEN(1)){
                
                removeChild(myactor);
                world->DestroyBody(b);
                
            }
            
        }
        
    }
    
}





void GameScene::menuCloseCallback(Ref* pSender)
{
    //Close the cocos2d-x game scene and quit the application
    Director::getInstance()->pushScene(MainMenuScene::createScene());
    
    /*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() as given above,instead trigger a custom event created in RootViewController.mm as below*/
    
    //EventCustom customEndEvent("game_scene_close_event");
    //_eventDispatcher->dispatchEvent(&customEndEvent);
    
    
}
bool isUp = false;

void GameScene::loadBall(){
auto ball = Sprite::create("Ball.png");
    if (ball == nullptr ||
        ball->getContentSize().width <= 0 ||
        ball->getContentSize().height <= 0)
    {
        problemLoading("'Ball.png'");
    }
    else{
        
        ball->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
        //   ball->setPhysicsBody(PhysicsBody::createCircle(ball->getContentSize().height/2));
        addChild(ball, 0);
        printf("We be ballin\n"); //if all goes well print that our ball is successful
    }
    
    /////physics
    b2BodyDef bodyDef;
    bodyDef.type = b2_dynamicBody;
    bodyDef.position = b2Vec2(ball->getPositionX(), ball->getPositionY());
    bodyDef.userData = ball;
    
    auto ballBody = world->CreateBody(&bodyDef);
    
    b2CircleShape circle;
    circle.m_radius = WORLD_TO_SCREEN(0.6);
    
    b2FixtureDef bodyFixDef;
    bodyFixDef.shape = &circle;
    bodyFixDef.density = 1.0f;
    bodyFixDef.friction = 0.6f;
    bodyFixDef.restitution = 0.0;
    
    ballBody->CreateFixture(&bodyFixDef);
}
